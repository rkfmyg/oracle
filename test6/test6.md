﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010414324    姓名：杨兴梁

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 1.创建表空间

```
Create Tablespace demo_temp
datafile
'D:\demo\orcl\demo_temp.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
Create Tablespace demo
datafile
'D:\demo\orcl\demo_user.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```

![](创建表空间.jpg)

#### 2.创建表

1.用户表

```
create table TB_USER
(
  id   INTEGER,
  no   VARCHAR2(50),
  pwd  VARCHAR2(50),
  name VARCHAR2(50),
  type CHAR(1)
)tablespace demo
```

![](创建用户表.jpg)

2.分类表

```
create table TB_CLASS
(
  id   INTEGER,
  name VARCHAR2(50)
)tablespace demo_temp
```

![](创建分类表.jpg)

3.商品表

```
create table TB_COMMODITY
(
 id      INTEGER,
 product_name VARCHAR2(100),
 classify_id INTEGER,
 model    VARCHAR2(50),
 unit     VARCHAR2(50),
 market_value VARCHAR2(50),
 sales_price VARCHAR2(50),
 cost_price  VARCHAR2(50),
 img     VARCHAR2(100),
 introduce  VARCHAR2(500),
 num     INTEGER
)tablespace demo_temp
```

![](创建商品表.jpg)

4.客户表

```
create table TB_CUSTOMER1
(
 id     INTEGER,
 customer_no VARCHAR2(50),
 name    VARCHAR2(50),
 phone    VARCHAR2(20),
 address   VARCHAR2(100)
)tablespace demo_temp
```

5.订单表

```
CREATE TABLE TB_ORDER1 (
order_id NUMBER(10),
product_id NUMBER(10),
quantity NUMBER(10),
price NUMBER(10,2),
CONSTRAINT order_details_primary_key PRIMARY KEY (order_id, product_id)
) TABLESPACE demo_temp;
```

![](创建订单表.jpg)

#### 3.插入数据

用户表

```
INSERT INTO "DEMO"."TB_USER" VALUES ('1', 'admin', '123', 'admin', '1');
INSERT INTO "DEMO"."TB_USER" VALUES ('2', 'yxl', '123', 'yxl', '2');
```

![](用户表插入信息.jpg)

分类表

```
--为用户表插入3w数据
INSERT INTO TB_CLASSIFY (ID,NAME)
SELECT level,  '服装' + mod(level, 1000)
FROM dual
CONNECT BY level <= 30000;
```

![](classify表插入数据.jpg)

商品表

```
--为商品表插入5w数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO TB_COMMODITY (id,product_name,classify_id,model,unit,market_value,sale_price,costprice,img,introduce,num)
    VALUES (i, 'Product ' || i, i,'bb','aa','00','10yuan','11yuan','src-img','intro',i);
  END LOOP;
  COMMIT;
END;
--商品表插入数据
```

客户表

```
-- 为客户表插入5w数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO TB_CUSTOMER (id, customer_no, name,phone,address)
    VALUES (i, 'cust('100',i)','张三 ' || i, FLOOR(dbms_random.value(1000000000,20000000000),concat('xxx地址00',i));
  END LOOP;
  COMMIT;
END
```

订单表

```
--为订单表插入2w数据
INSERT INTO TB_ORDER1 (order_id, product_id, quantity, price)
SELECT level, mod(level, 1000)+1, mod(level, 10)+1, mod(level, 100)+1
FROM dual
CONNECT BY level <= 20000;
```

![](插入数据结果.jpg)

#### 4.设计权限及用户分配

```
 --创建管理员用户
CREATE TB_USER admin IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE, DBA TO admin;
 --创建普通用户
CREATE TB_USER yxl IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE TO yxl;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO yxl;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO yxl;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO yxl;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO yxl;
```

#### 5.PL/SQL设计

1.创建包

定义了以下过程和函数：取消客户订单cancel_order、

计算订单总金额calculate_order_total、

根据供应商ID查询供应商信息get_supplier_info

```
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg AS
   
    -- 存储过程：取消客户订单
    PROCEDURE cancel_order(
        p_order_id IN NUMBER
    );
    -- 计算订单总金额
	FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER;
   -- 根据供应商ID查询供应商信息
	FUNCTION get_supplier_info(supplier_id IN NUMBER) RETURN supplier%ROWTYPE;
END sales_pkg;
```

![](创建程序包.jpg)

2.计算商品总价

```
DECLARE
  calculate_order_total NUMBER;
BEGIN
  calculate_order_total := MANAGEMENT.calculate_order_total(p_order_id => 1);
  DBMS_OUTPUT.PUT_LINE('订单总金额：' || calculate_order_total);
END;
```

![](计算函数的结果.jpg)



#### 6.备份方案

1.增量导出／导入方案

利用Export可将数据从数据库中提取出来，利用Import则可将提取出来的数据送回到Oracle数据库中去。

> 增量导出是一种常用的数据备份方法，它只能对整个数据库来实施，并且必须作为SYSTEM来导出。在进行此种导出时，系统不要求回答任何问题。导出文件名缺省为export.dmp，如果不希望自己的输出文件定名为export.dmp，必须在命令行中指出要用的文件名。
> 增量导出包括三种类型：
> （１）、“完全”增量导出（Complete）
> 即备份三个数据库，比如：
> exp system/manager inctype=complete file=‘D:\demo\orcl\demo_user.dmp’
> （２）、“增量型”增量导出
> 备份上一次备份后改变的数据，比如：
> exp system/manager inctype=incremental file=040731.dmp

2.热备份--RMAN 备份

> ​    1）、目标数据库打开归档模式
> ​	archive log list;
> ​	shutdown immediate;
> ​	startup mount;
> ​	alter database archivelog;
> ​	alter database open;
> ​	archive log list;
> ​    2 ）、建立恢复目录并注册目标数据库
> ​      1>. 使用 dbca 创建恢复目录数据库 rcat：
> ​          配置要在其中存储恢复目录的数据库。
> ​       CREATE TABLESPACE rcat_ts DATAFILE size 15M;
> ​          创建恢复目录所有者。
> ​       CREATE USER rcowner IDENTIFIED BY rcpass
> ​        TEMPORARY TABLESPACE temp
> ​        DEFAULT TABLESPACE rcat_ts
> ​        QUOTA UNLIMITED ON rcat_ts;
> ​       GRENT recovery_catalog_owner TO rcowner;  (授予角色）
> ​          创建恢复目录。
> ​        $rman
> ​        RMAN>CONNECT CATALOG username/password@net_service_name(以目录所有者的身份连接到恢复目录数据库）
> ​        RMAN>CREATE CATALOG;(执行CREATE CATALOG命令）
> ​     2>、 在 rcat 中创建恢复目录所用表空间：
> ​        使用 sqlplus 以 sysdba 权限连接到 rcat, 创建 rcat 表空间：
> ​         . oraenv
> ​         rcat
> ​         sqlplus / as sysdba
> ​         select instance_name, status from v$instance;
> ​         create tablespace rcat datafile '+DATA/ract01.dbf' size 100 M;
> ​     3>. 创建目录数据库的所有者用户 rcatowner：
> ​         create user rcatowner identified by oracle_4U
> ​           default tablespace rcat temporary tablespace tmp
> ​           quota unlimited on rcat;
> ​         grant connect, resource, recovery_catalog_owner to rcatowner;
> ​         exit
> ​     4>. 创建恢复目录：
> ​        使用 RMAN 以恢复目录所有者 rcatowner 连接到 rcat 目录数据库。
> ​         rman catalog rcatowner/oracle_4U@rcat
> ​         create catalog;
> ​         exit
> ​     5>. 在恢复目录数据库中注册目标数据库：
> ​         . oraenv
> ​         orcl
> ​         rman target / catalog rcatowner@rcat
> ​         register database;
> ​      6>．检查是否注册成功：
> ​          list incarnation;
> ​      7>．检查同步后恢复目录信息：
> ​           report schema;

### 总结

通过这次实验我再次使用了创建用户、表空间、表和存储过程/函数，以及并且设计了一套数据库备份方案。在这次综合的Oracle数据库的商品销售系统我创建了四个表：用户表（TB_USER）分类表 (TB_CLASS)，商品表 (TB_COMMODITY)，订单表 (TB_ORDER1)，客户表 (TB_CUSTOMER1)。这些表定义了数据库模式中不同实体之间的关系，通过主键和外键约束确保数据的完整性和一致性。

随后我为表虚拟了大量的数据。使其模仿真实的使用场景。

然后设计了用户权限分配方案，我们授予 admin 用户 DBA 角色，即数据库管理员角色，而给 yxl 用户授予 connect 和 resource 角色，以及访问表的权限，使其能够操作数据库。

其次创建了一个名为 sales_pkg 的程序包，定义了以下过程和函数：取消客户订单cancel_order、计算订单总金calculate_order_total、根据供应商ID查询供应商信息get_supplier_info。

跟个数据库系统设计还是有很多不足，在实际应用中表以及关系偏少。系统的数据结构和业务逻辑比较牵强，但是能够有效地处理订单、库存和供应商等相关操作。在将来的学习中将继续丰富相关知识，将课程所学的内容不断开枝散叶。

